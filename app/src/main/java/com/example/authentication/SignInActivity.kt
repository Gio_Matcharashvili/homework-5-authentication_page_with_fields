package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signIn2Button.setOnClickListener{
            SignIn()
        }
    }
    private fun SignIn(){
        val email = email2EditText.text.toString()
        val password = password2EditText.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty()){
            signIn2Button.isClickable = false
            signInProgressBar.visibility = View.VISIBLE

            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->

                    signInProgressBar.visibility = View.GONE
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        Toast.makeText(this, "Authentication is Success!", Toast.LENGTH_SHORT).show()
                        val ProIntent = Intent(this, ProfileActivity::class.java)
                        startActivity(ProIntent)
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(this, "Authentication failed.", Toast.LENGTH_SHORT).show()
                    }
                    signIn2Button.isClickable = true
                }

        }else{
            Toast.makeText(this, "Please fill all fields.", Toast.LENGTH_SHORT).show()
        }


    }

}